﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitMapper
{
    class Program
    {
        static void Main(string[] args)
        {
            var mapper = new CharMapper();

            readline:
            Console.WriteLine();
            Console.WriteLine("Write a word/sentence with special characters (  !()$&^#*%@   )");
            var chars = Console.ReadLine();
            var IsCharsValid =IsValidInput(chars);
            while (String.IsNullOrWhiteSpace(chars) || !IsCharsValid )
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You can't leave spaces blank or without special characters");
                Console.ResetColor();
                goto readline;

            }

            

             mapper.Mapper(chars);

            
        }

        public static bool IsValidInput(string words)
        {
            var count = 0;
            var specialCharacters = "! ( ) $ & ^ # * % @";
            foreach (var item in specialCharacters.Split())
            {
                
                if (words.Contains(item))
                    count++;             
            }
            if (count > 0)
                return true;
            return false;
        }
    }
}
