﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitMapper
{
    class CharMapper
    {
        private string SpecialChars = ")(*&^%$#@!";

        private Dictionary<char, string> SpecialCharacters = new Dictionary<char, string>() { 
            
            
            { ')', "1" },
            {'(', "2" },
            {'*', "3"}, 
            {'&', "4"}, 
            {'^', "5"},
            {'%', "6"},
            {'$', "7"}, 
            {'#', "8"}, 
            {'@', "9"},
            {'!', "0"},
           { ' ', " " }
        };
        public void Mapper(string chars)
        {
            

            var querySyntaxChars = from sChar in chars
                                   where SpecialCharacters.ContainsKey(sChar)
                                   select SpecialCharacters[sChar];


            Console.WriteLine();
            Console.WriteLine("Query syntax Output: ");
            foreach (var item in querySyntaxChars)
            {
               
                Console.Write(item);
                
            }


            Console.WriteLine();
                Console.WriteLine("------------------------------");
                Console.WriteLine("Method syntax Output");

                var methodSyntaxChars = chars.Where(validChar => SpecialCharacters.ContainsKey(validChar)).Select(u=> SpecialCharacters[u]);
                foreach (var item in methodSyntaxChars)
                {
                Console.Write(item);
                 }

                Console.WriteLine();
            }
        }
}
