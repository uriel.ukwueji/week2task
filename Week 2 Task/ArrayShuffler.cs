﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_2_Task.Array_Shuffler
{
    class ArrayShuffler
    {
        
        public void ShuffleArray()

        {

            var randomGenerator = new Random();
            //var sortedWords = new List<string> { "abel", "and", "between", "cream", "foo", "hero", "one", "zero"};
            var sortedWords = new SortedList<string, string>() {
                {"1", "abel" }, {"2", "and"}, {"3", "between" }, {"4", "cream"}, {"5", "hero"}, {"6", "zero"}
                };
            
           
            var querySyntaxShuffler = from word in sortedWords
                           orderby randomGenerator.Next()
                           select word;
            try
            {
                Console.WriteLine("Query Syntax Output");
                Console.ForegroundColor = ConsoleColor.Cyan;
                foreach (var item in querySyntaxShuffler)
                {
                    Console.Write($"{item.Value} ");
                }
                Console.ResetColor();
                Console.WriteLine();
                Console.WriteLine();
            }

            
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            var methodSyntaxShuffler = sortedWords.OrderBy(word => randomGenerator.Next());
            try
            {
                Console.WriteLine("Method Syntax Output");
                Console.ForegroundColor = ConsoleColor.Cyan;
                foreach (var item in methodSyntaxShuffler)
                {
                    Console.Write($"{item.Value} ");
                }
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

        }
    }
}
