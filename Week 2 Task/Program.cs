﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_2_Task
{


    class Program
    {
        static void Main(string[] args)
        {

            var arrayShuffler = new Array_Shuffler.ArrayShuffler();

            arrayShuffler.ShuffleArray();
            var  input = Display();

            while (input != "1" && input !="2")
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("INVALID SELECTION");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Press 1 to Reshuffle   Press 2 to End");
                Console.ResetColor();
                input = Console.ReadLine();
            }
            if (input == "1")
            {
                do
                {
                    Console.Clear();
                    arrayShuffler.ShuffleArray();
                    input = Display();
                }

                while (input == "1");

            }

            else
            {
                do
                {
                    return;
                }

                while (input == "2");
            }
        }

        public static string Display()
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Press 1 to Reshuffle");
            Console.WriteLine("Press 2 to End Application");
            Console.ResetColor();
            var input = Console.ReadLine();
            return input;
        }
    }
}
