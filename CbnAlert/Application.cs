﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CbnAlert
{
    static class Application
    {
        private static string _IsLoggedIn = "false";
        public static void Run()
        {
             var cbn = new Cbn();
            AccountService.BannedUserAlert += cbn.BannedUserInfo; // subscribing to publisher
            Console.WriteLine();
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("WELCOME TO CBN SERVICE");
            Console.BackgroundColor = ConsoleColor.Black;

            do
            {
               
                Menu();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine();
                Console.WriteLine("...GOODBYE...");
                System.Threading.Thread.Sleep(6000);
                Console.ResetColor();
                Console.Clear();
            }

            while (_IsLoggedIn == "true");
           
        }

        public static void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("What will you like to do");
            Console.WriteLine();
            Console.WriteLine();
            var action = ToDo("Enroll Bvn", "Check Bvn");

            Console.WriteLine("Enter your Name: ");
            var name = Console.ReadLine();
            while (IsBlank(name) || name.Split().Length >1 || name.Split(',').Length > 1)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Field cannot be empty or multiple words");
                Console.ResetColor();
                Console.WriteLine("Enter your Name: ");
                name = Console.ReadLine();
            }

            Console.WriteLine("Enter your  Account Number: ");
            var accNumber = Console.ReadLine();
            while (IsBlank(accNumber) || accNumber.Split().Length > 1 || accNumber.Split(',').Length > 1)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Field cannot be empty or multiple words");
                Console.ResetColor();
                Console.WriteLine("Enter your  Account Number: ");
                accNumber = Console.ReadLine();
            }

            _IsLoggedIn = AccountService.Login(name, accNumber);
            while (_IsLoggedIn == "false")
            {

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("User not Found");
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine("Enter your Name: ");
                var tempName = Console.ReadLine();

                Console.WriteLine("Enter your  Account Number: ");
                var tempAccNumber = Console.ReadLine();

                _IsLoggedIn = AccountService.Login(tempName, tempAccNumber);
            }

            while (_IsLoggedIn == "banned")
            {

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Restricted User cannot Log in");
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine("Enter your Name: ");
                var tempName = Console.ReadLine();

                Console.WriteLine("Enter your  Account Number: ");
                var tempAccNumber = Console.ReadLine();

                _IsLoggedIn = AccountService.Login(tempName, tempAccNumber);
            }


            if (action == "1")
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine();
                    Console.WriteLine("Enrolling Bvn...");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine();


                    AccountService.EnrollBvn(name, accNumber);
                }

                else if (action == "2")
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine();
                    Console.WriteLine("Checking Bvn...");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine();

                    AccountService.CheckBvn(name, accNumber);
                }

                
        }

        public static bool IsBlank(string input)
        {
            if (String.IsNullOrWhiteSpace(input))
                return true;
            return false;
        }

        public static string ToDo(string action1, string action2)
        {
            Console.WriteLine("Press");
            Console.WriteLine($"1: {action1}  2:  {action2}");
            var action = Console.ReadLine();

            while(action !="1" && action != "2")
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid Selection");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Press");
                Console.WriteLine($"1: {action1}  2:  {action2}");

                action = Console.ReadLine();
            }
            return action;
            
        }
    }
}
