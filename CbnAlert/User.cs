﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CbnAlert
{
    public class User
    {
        public string FirstName { get; set; }
        public string BVN { get; set; }
        public string AccNumber { get; set; }
    }
}
