﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CbnAlert
{
   public static class AccountService
    {
        //publisher of an event will contain the OnEventName method that invokes the event
        //and because this OnEventName method is always protected another method should be used -
        // to call it 
        public static event EventHandler<string> BannedUserAlert; // publisher
        private static List<User>  allUsers = new List<User>() {
                    new User{BVN = "0124859503939", FirstName = "james", AccNumber = "3012343323"},
                    new User { BVN = "0124859503940", FirstName = "jane", AccNumber = "3024343323" },
                    new User { BVN = "01248595039", FirstName = "bill" , AccNumber = "3065343354"},
                    new User { BVN = "01248595039", FirstName = "chikki" , AccNumber = "5065341259"},
                    
                    new User { FirstName = "dara" , AccNumber = "4065343354"},
                    new User {  FirstName = "alex" , AccNumber = "7153433541"},
                    new User { FirstName = "loveth" , AccNumber = "6035343565"}
    };
        private static long _bvn = 1234567890;

       
        public static string Login(string name, string accountNumber)
        {
            name = name.Trim().ToLower();
            accountNumber = accountNumber.Trim();
            var valid = allUsers.SingleOrDefault(n =>  n.FirstName == name && n.AccNumber == accountNumber);
            if (valid != null)
                {
                     if (IsUserBanned(accountNumber))
                     {
                            Console.Beep();
                            Console.Beep();
                            OnBannedUserLogin(name, accountNumber);
                            return "banned";
                           
                     }
                    return "true";
                }
            return "false";   
            

        }

        static void OnBannedUserLogin(string name, string accountNumber)
        {
            BannedUserAlert?.Invoke(name, accountNumber);
        }

        private  static bool IsUserBanned(string accountNumber)
        {
            accountNumber = accountNumber.Trim();
            if (accountNumber == "3012343323" || accountNumber == "3024343323" || accountNumber == "3065343354")
                return true;
            return false;
        }


        public static void CheckBvn(string name, string accountNumber)
        {
            var  users = allUsers.Where(n => n.AccNumber == accountNumber);
            foreach (var item in users)
            {
                if(item.BVN == null)
                {
                    Console.WriteLine(" You have not  been Enrolled ");
                    Console.WriteLine("Press 1 to enroll");
                    var input = Console.ReadLine();

                    while(input != "1")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("INVALID INPUT!!!");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Enter a Valid Selection");
                        input = Console.ReadLine();

                    }

                    Console.WriteLine();

                    EnrollBvn(name, accountNumber);

                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Hello {item.FirstName}, This is your BVN number: {item.BVN} ");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            
  
        }


        public static void EnrollBvn(string name, string accNumber)
        {

           var users = allUsers.Where(n => n.FirstName == name && n.AccNumber == accNumber);

            foreach (var user  in users)
            {
                if(user.BVN != null)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"You have a BVN number already");
                    Console.WriteLine($"BVN:  {user.BVN}");
                    Console.ForegroundColor = ConsoleColor.White;
                    return;
                }
                user.BVN = Convert.ToString(_bvn);
                _bvn++;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"Bvn enrollment Successful");
                Console.WriteLine($"BVN:  {user.BVN}");
                Console.ForegroundColor = ConsoleColor.White;
            }

            

        }


    }
}
