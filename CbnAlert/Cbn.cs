﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CbnAlert
{
    class Cbn
    {
        //subcriber describes the message to be sent 

        public void BannedUserInfo(object sender, string accNumber)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"Banned User {sender} with Account Number: {accNumber} attempted to log in");
            Console.ResetColor();
        }
    }
}
